<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Answer;
use App\Story;
use App\User;
use Faker\Generator as Faker;

$factory->define( Answer::class, function ( Faker $faker ) {
    return [
        'content'  => $faker->paragraph,
        'rate'     => $faker->randomNumber,
        'user_id'  => function () {
            return factory( User::class )->create()->id;
        },
        'story_id' => function () {
            return factory( Story::class )->create()->id;
        },
    ];
} );
