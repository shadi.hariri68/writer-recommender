<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Story;
use Faker\Generator as Faker;

$factory->define( Story::class, function ( Faker $faker ) {
    return [
        'title'       => $faker->sentence,
        'description' => $faker->paragraph,
        'ad'          => $faker->sentence,
        'will_rate'       => $faker->boolean,
        'level' => $faker->randomElement(Story::LEVELS),
        'user_id' => function () {
            return factory( App\User::class )->create()->id;
        },
        'language_id' => function () {
            return factory( App\Language::class )->create()->id;
        },
    ];
} );

