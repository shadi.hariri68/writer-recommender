<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create( 'stories', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'title' );
            $table->string( 'level' );
            $table->text( 'description' );
            $table->string( 'ad' );
            $table->boolean( 'will_rate' )->default( false );
            $table->bigInteger( 'user_id' );
            $table->bigInteger( 'language_id' );
            $table->timestamps();
        } );
        Schema::table( 'stories', function ( Blueprint $table ) {
            $table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );
            $table->foreign( 'language_id' )->references( 'id' )->on( 'languages' )->onDelete( 'cascade' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'stories' );
    }
}
