<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'answers', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->bigInteger( 'user_id' );
            $table->bigInteger( 'story_id' );
            $table->text( 'content' );
            $table->integer( 'rate' )->default( 0 );
            $table->timestamps();
        } );
        Schema::table( 'answers', function ( Blueprint $table ) {
            $table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );
            $table->foreign( 'story_id' )->references( 'id' )->on( 'stories' )->onDelete( 'cascade' );
        } );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'answers' );
    }
}
