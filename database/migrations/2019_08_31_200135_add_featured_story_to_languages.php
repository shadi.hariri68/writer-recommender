<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeaturedStoryToLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'languages', function ( Blueprint $table ) {
            $table->bigInteger( 'featured_story' )->nullable();
        } );
        Schema::table( 'languages', function ( Blueprint $table ) {
            $table->foreign( 'featured_story' )->references( 'id' )->on( 'stories' )->onDelete( 'cascade' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'languages', function ( Blueprint $table ) {
            $table->dropColumn( 'featured_story' );
        } );
    }
}
