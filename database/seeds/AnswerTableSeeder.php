<?php

use Illuminate\Database\Seeder;
use App\Story;
use App\Answer;
use App\User;
class AnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stories = Story::all();
        foreach ($stories as $story){
            factory( Answer::class, 5 )->create( [ 'story_id' => $story->id, 'user_id' => factory( User::class )->create()->id] );

        }
    }
}
