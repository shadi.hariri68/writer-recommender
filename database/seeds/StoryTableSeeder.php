<?php

use App\Story;
use Illuminate\Database\Seeder;

class StoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $french = \App\Language::where( 'slug', '=', 'french' )->first();
        $english = \App\Language::where( 'slug', '=', 'english' )->first();
        factory( Story::class, 5 )->create( [ 'language_id' => $french->id ] );
        factory( Story::class, 5 )->create( [ 'language_id' => $english->id ] );


    }
}
