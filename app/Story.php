<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{

    const LEVEL_SIMPLE = 'simple';
    const LEVEL_INTERMEDIATE = 'intermediate';
    const LEVEL_ADVANCED = 'advanced';

    const LEVELS = [ self::LEVEL_SIMPLE, self::LEVEL_INTERMEDIATE, self::LEVEL_ADVANCED ];

    protected $fillable = [ 'title', 'description', 'ad', 'will_rate', 'level', 'user_id', 'language_id' ];

    public function language()
    {
        return $this->belongsTo( 'App\Language' );
    }

    public function user()
    {
        return $this->belongsTo( 'App\User' );
    }

    public function answers()
    {
        return $this->hasMany( 'App\Answer' );
    }
}
