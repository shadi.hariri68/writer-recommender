<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Language extends Model
{
    protected $fillable = [ 'name', 'description', 'summary', 'image', 'slug', 'featured_story' ];

    public static function boot()
    {
        parent::boot();

        static::saving( function ( $model ) {
            $model->slug = Str::slug( $model->name );
        } );
    }

    public function stories()
    {
        return $this->hasMany( 'App\Story' );
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function story()
    {
        return $this->belongsTo( 'App\Story', 'featured_story' );
    }
}
