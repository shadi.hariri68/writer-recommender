<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [ 'user_id', 'story_id', 'content', 'rate' ];

    public function story()
    {
        return $this->belongsTo( 'App\Story' );
    }

    public function user()
    {
        return $this->belongsTo( 'App\User' );
    }
}
