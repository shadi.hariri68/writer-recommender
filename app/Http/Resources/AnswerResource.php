<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class AnswerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray( $request )
    {
        $story = $this->story;
        if ( Auth::user() ) {
            $canRate = $story->will_rate && ( Auth::user()->id === $story->user_id );

        } else {
            $canRate = false;
        }

        return [
            'id'         => $this->id,
            'content'    => $this->content,
            'rate'       => $this->rate,
            'user'       => new UserResource( $this->user ),
            'story'      => new StoryResource( $story ),
            'can_rate'   => $canRate,
            'created_at' => Carbon::parse( $this->created_at )->toDateString(),
            'updated_at' => Carbon::parse( $this->updated_at )->toDateString(),
        ];
    }
}
