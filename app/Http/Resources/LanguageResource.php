<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class LanguageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray( $request )
    {
        return [
            'id'             => $this->id,
            'name'           => $this->name,
            'featured_story' => new StoryResource($this->whenLoaded( 'stories' )),
            'slug'           => $this->slug,
            'description'    => $this->description,
            'summary'        => $this->summary,
            'image'          => $this->image,
            'created_at'     => Carbon::parse( $this->created_at )->toDateString(),
            'updated_at'     => Carbon::parse( $this->updated_at )->toDateString(),
        ];
    }
}
