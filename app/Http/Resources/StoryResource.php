<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class StoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray( $request )
    {
        return [
            'id'          => $this->id,
            'title'       => $this->title,
            'description' => $this->description,
            'level'       => $this->level,
            'ad'          => $this->ad,
            'language'    => new LanguageResource( $this->language ),
            'answers'     => AnswerResource::collection( $this->whenLoaded( 'answers' ) ),
            'user'        => new UserResource( $this->user ),
            'will_rate'   => (boolean) $this->will_rate,
            'created_at'  => Carbon::parse( $this->created_at )->toDateString(),
            'updated_at'  => Carbon::parse( $this->updated_at )->toDateString(),
        ];
    }
}
