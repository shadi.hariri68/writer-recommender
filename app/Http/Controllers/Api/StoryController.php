<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\StoryCollection;
use App\Http\Resources\StoryResource;
use App\Language;
use App\Story;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class StoryController extends BaseController
{
    public function __construct()
    {
        $this->middleware( 'auth:api', [ 'except' => [ 'index', 'show' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Language $language )
    {

        $stories = $language->stories()->paginate( config( 'app.pagination_limit' ) );

        return $this->sendResponse( new StoryCollection( $stories ), 'Stories retrieved successfully.' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Language $language, Request $request )
    {
        $validator = Validator::make( $request->all(), [
            'title'       => 'required',
            'description' => 'required',
            'level'       => 'required',
        ] );


        if ( $validator->fails() ) {
            return $this->sendErrorRequestNotValid( 'Validation Error.', $validator->errors() );
        }

        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $input['language_id'] = $language->id;
        $story = Story::create( $input );

        return $this->sendResponse( new StoryResource( $story ), 'Story stored successfully.' );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Language $language, Story $story )
    {
        $story = $story->load('answers');
        return $this->sendResponse( new StoryResource( $story ), 'story retrieved successfully.' );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, Language $language, Story $story )
    {

        $story->update( $request->all() );

        return $this->sendResponse( new StoryResource( $story ), 'Story updated successfully.' );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( Language $language, Story $story )
    {
        $story->delete();

        return $this->sendResponse( $story->id, 'Story deleted successfully.' );

    }
}
