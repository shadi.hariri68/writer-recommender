<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\LanguageCollection;
use App\Http\Resources\LanguageResource;
use App\Language;
use Illuminate\Http\Request;
use Validator;

class LanguageController extends BaseController
{
    public function __construct()
    {
        $this->middleware( 'auth:api', [ 'except' => [ 'index', 'show' ] ] );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = Language::paginate( config( 'app.pagination_limit' ) );


        return $this->sendResponse( new LanguageCollection( $languages ), 'Languages retrieved successfully.' );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $validator = Validator::make( $request->all(), [
            'name'        => 'required|unique:languages',
            'image'       => 'required',
            'description' => 'required',
            'summary'     => 'required',
            'featured_story' => 'exists:stories,id'

        ] );


        if ( $validator->fails() ) {
            return $this->sendErrorRequestNotValid( 'Validation Error.', $validator->errors() );
        }

        $input = $request->all();
        $language = Language::create( $input );


        return $this->sendResponse( new LanguageResource( $language ), 'Language stored successfully.' );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show( Language $language )
    {
        $language = $language->load('story');
        return $this->sendResponse( new LanguageResource($language ), 'Language retrieved successfully.' );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, Language $language )
    {
        $validator = Validator::make( $request->all(), [
            'name' => 'unique:languages',
            'featured_story' => 'exists:stories,id'
        ] );


        if ( $validator->fails() ) {
            return $this->sendErrorRequestNotValid( 'Validation Error.', $validator->errors() );
        }
        $language->update( $request->all() );

        return $this->sendResponse( new LanguageResource( $language ), 'Language updated successfully.' );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( Language $language )
    {
        $language->delete();

        return $this->sendResponse( $language->id, 'Language deleted successfully.' );
    }
}
