<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
class AuthController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    public function register( Request $request )
    {

        $validator = Validator::make( $request->all(), [

            'name'     => 'required',
            'email'    => 'required|email|unique:users',
            'password' => 'required',

        ] );


        if ( $validator->fails() ) {
            return $this->sendErrorRequestNotValid( 'Validation Error.', $validator->errors() );
        }


        $input = $request->all();
        $input['password'] = bcrypt( $input['password'] );
        $user = User::create( $input );
        $success['access_token'] = $user->createToken( 'KetabroHero' )->accessToken;
        $success['user_id'] = $user->id;
        $success['name'] = $user->name;

        return $this->sendResponse( $success, 'User register successfully.' );

    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     *
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login( Request $request )
    {
        $request->validate( [
            'email'       => 'required|string|email',
            'password'    => 'required|string',
            'remember_me' => 'boolean'
        ] );
        $credentials = request( [ 'email', 'password' ] );
        if ( ! Auth::attempt( $credentials ) ) {
            return $this->sendErrorRequestNotValid( 'Request is not Authorized' );
        }
        $user = $request->user();
        $tokenResult = $user->createToken( 'Personal Access Token' );
        $token = $tokenResult->token;
        if ( $request->remember_me ) {
            $token->expires_at = Carbon::now()->addWeeks( 1 );
        }
        $token->save();
        return $this->sendResponse( [
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 'User login successfully.' );

    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout( Request $request )
    {
        $request->user()->token()->revoke();
        return $this->sendResponse( [], 'User logout successfully.' );
    }
}
