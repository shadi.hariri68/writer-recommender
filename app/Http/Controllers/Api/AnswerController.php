<?php

namespace App\Http\Controllers\API;

use App\Answer;
use App\Http\Resources\AnswerCollection;
use App\Http\Resources\AnswerResource;
use App\Language;
use App\Story;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class AnswerController extends BaseController
{
    public function __construct()
    {
        $this->middleware( 'auth:api', [ 'except' => [ 'index', 'show' ] ] );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Language $language, Story $story)
    {
        $answers = $story->answers()->paginate( config( 'app.pagination_limit' ) );

        return $this->sendResponse( new AnswerCollection($answers ), 'Answers retrieved successfully.' );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Language $language, Story $story,Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'content'       => 'required',
        ] );


        if ( $validator->fails() ) {
            return $this->sendErrorRequestNotValid( 'Validation Error.', $validator->errors() );
        }
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $input['story_id'] = $story->id;
        $answer = Answer::create( $input );
        return $this->sendResponse( new AnswerResource( $answer ), 'Answer stored successfully.' );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Language $language, Story $story, Answer $answer)
    {
        return $this->sendResponse( new AnswerResource( $answer ), 'Answer retrieved successfully.' );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Language $language, Story $story,Request $request, Answer $answer)
    {
        $answer->update( $request->all() );
        return $this->sendResponse( new AnswerResource( $answer ), 'Answer updated successfully.' );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language, Story $story,Request $request, Answer $answer)
    {
        $answer->delete();

        return $this->sendResponse( $answer->id, 'Answer deleted successfully.' );
    }
}
