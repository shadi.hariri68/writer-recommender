<?php

namespace Tests\Unit;

use App\Http\Resources\BookResource;
use App\Http\Resources\HubResource;
use App\Http\Resources\LanguageResource;
use App\Language;
use App\Story;
use App\User;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LanguageControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Index test
     *
     * @return void
     */
    public function testIndex()
    {

        $language = factory( Language::class )->create();
        $this->get( '/api/languages' )->assertJsonFragment( [
            'id'          => $language->id,
            'name'        => $language->name,
            'slug'        => $language->slug,
            'description' => $language->description,
            'summary'     => $language->summary,
            'image'       => $language->image,
            'created_at'  => Carbon::parse( $language->created_at )->toDateString(),
            'updated_at'  => Carbon::parse( $language->updated_at )->toDateString(),
        ] );
    }

    /**
     * Show test
     *
     * @return void
     */
    public function testShow()
    {
        $language = factory( Language::class )->make();
        $featured_story = factory( Story::class )->make();
        $featured_story->save();
        $language->featured_story = $featured_story->id;
        $language->save();
        $language->load( 'story' );
        $expected = json_encode( Arr::sortRecursive( [
                'data' => new LanguageResource( $language )
            ]

        ) );
        $this->get( '/api/languages/' . $language->slug )->assertJsonFragment(
            json_decode( $expected, true )
        );
    }

    /**
     * Store test
     *
     * @return void
     */
    public function testStore()
    {

        Passport::actingAs(
            $user = factory( User::class )->create()
        );
        $language = factory( Language::class )->make();

        $this->post( '/api/languages', [
            'name'        => $language->name,
            'description' => $language->description,
            'summary'     => $language->summary,
            'ad'          => $language->ad,
            'image'       => $language->image,

        ] )->assertJsonFragment( [
            'message' =>
                'Language stored successfully.'

        ] );
        $this->assertDatabaseHas( 'languages', [
            'name' => $language->name,
        ] );

    }

    /**
     * Update test
     *
     * @return void
     */
    public function testUpdate()
    {

        Passport::actingAs(
            $user = factory( User::class )->create()
        );
        $language = factory( Language::class )->create();

        $faker = Faker::create();
        $newDescription = $faker->sentence;
        $this->put( '/api/languages/' . $language->slug, [
            'description' => $newDescription
        ] )->assertJsonFragment( [
            'message' =>
                'Language updated successfully.'

        ] );
        $this->assertDatabaseHas( 'languages', [
            'description' => $newDescription
        ] );
    }
    /**
     * Delete test
     *
     * @return void
     */
    public function testDelete()
    {

        Passport::actingAs(
            $user = factory( User::class )->create()
        );
        $language = factory( Language::class )->create();

        $this->delete( '/api/languages/' . $language->slug )->assertJsonFragment( [
            'message' =>
                'Language deleted successfully.'

        ] );
        $this->assertDatabaseMissing( 'languages', [
            'id' => $language->id,
        ] );
    }

}
