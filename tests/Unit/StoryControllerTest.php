<?php

namespace Tests\Unit;

use App\Http\Resources\LanguageResource;
use App\Http\Resources\StoryResource;
use App\Language;
use App\Story;
use App\User;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoryControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Index test
     *
     * @return void
     */
    public function testIndex()
    {

        $story = factory( Story::class )->create( );
        $expected = json_encode( Arr::sortRecursive( [
                'data' => [ new StoryResource( $story ) ]
            ]

        ) );
        $this->get( '/api/languages/' . $story->language->slug . '/stories' )
             ->assertJsonFragment( json_decode( $expected, true ) );
    }

    /**
     * Show test
     *
     * @return void
     */
    public function testShow()
    {
        $story = factory( Story::class )->create( );
        $story->load( 'answers' );

        $expected = json_encode( Arr::sortRecursive( [
                'data' => new StoryResource( $story )
            ]

        ) );
        $this->get( '/api/languages/' . $story->language->slug . '/stories/' . $story->id )->assertJsonFragment(
            json_decode( $expected, true )
        );
    }

    /**
     * Store test
     *
     * @return void
     */
    public function testStore()
    {

        Passport::actingAs(
            $user = factory( User::class )->create()
        );
        $story = factory( Story::class )->make(  );
        $this->post( '/api/languages/' . $story->language->slug . '/stories/', [
            'title'       => $story->title,
            'description' => $story->description,
            'level'       => $story->level,
            'ad'          => $story->ad,

        ] )->assertJsonFragment( [
            'message' =>
                'Story stored successfully.'

        ] );
        $this->assertDatabaseHas( 'stories', [
            'title' => $story->title,
        ] );

    }

    /**
     * Update test
     *
     * @return void
     */
    public function testUpdate()
    {
        Passport::actingAs(
            $user = factory( User::class )->create()
        );
        $story = factory( Story::class )->create();
        $faker = Faker::create();
        $newDescription = $faker->sentence;
        $this->put( '/api/languages/' . $story->language->slug . '/stories/'.$story->id, [
            'description' => $newDescription,


        ] )->assertJsonFragment( [
            'message' =>
                'Story updated successfully.'

        ] );
        $this->assertDatabaseHas( 'stories', [
            'description' => $newDescription,
        ] );

    }

    /**
     * Delete test
     *
     * @return void
     */
    public function testDelete()
    {

        Passport::actingAs(
            $user = factory( User::class )->create()
        );
        $story = factory( Story::class )->create();

        $this->delete( '/api/languages/' . $story->language->slug.'/stories/'.$story->id )->assertJsonFragment( [
            'message' =>
                'Story deleted successfully.'

        ] );
        $this->assertDatabaseMissing( 'stories', [
            'id' => $story->id,
        ] );
    }
}
