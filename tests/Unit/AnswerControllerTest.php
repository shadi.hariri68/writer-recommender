<?php

namespace Tests\Unit;

use App\Answer;
use App\Http\Resources\AnswerResource;
use App\Http\Resources\StoryResource;
use App\Language;
use App\Story;
use App\User;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AnswerControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Index test
     *
     * @return void
     */
    public function testIndex()
    {

        $answer = factory( Answer::class )->create();
        $expected = json_encode( Arr::sortRecursive( [
                'data' => [ new AnswerResource( $answer ) ]
            ]

        ) );
        $this->get( '/api/languages/' . $answer->story->language->slug
                    . '/stories/' . $answer->story->id . '/answers' )
             ->assertJsonFragment( json_decode( $expected, true ) );
    }

    /**
     * Show test
     *
     * @return void
     */
    public function testShow()
    {
        $answer = factory( Answer::class )->create();
        $expected = json_encode( Arr::sortRecursive( [
                'data' =>  new AnswerResource( $answer )
            ]

        ) );
        $this->get( '/api/languages/' . $answer->story->language->slug . '/stories/' . $answer->story->id.'/answers/'.$answer->id )->assertJsonFragment(
            json_decode( $expected, true )
        );
    }

    /**
     * Store test
     *
     * @return void
     */
    public function testStore()
    {

        Passport::actingAs(
            $user = factory( User::class )->create()
        );
        $answer = factory( Answer::class )->make();
        $this->post( '/api/languages/' . $answer->story->language->slug . '/stories/'.$answer->story->id.'/answers', [
            'content'       => $answer->content,

        ] )->assertJsonFragment( [
            'message' =>
                'Answer stored successfully.'

        ] );
        $this->assertDatabaseHas( 'answers', [
            'content' => $answer->content,
        ] );

    }

    /**
     * Update test
     *
     * @return void
     */
    public function testUpdate()
    {
        Passport::actingAs(
            $user = factory( User::class )->create()
        );
        $answer = factory( Answer::class )->create();
        $faker = Faker::create();
        $newContent = $faker->paragraph;
        $this->put( '/api/languages/' . $answer->story->language->slug . '/stories/' . $answer->story->id.'/answers/'.$answer->id, [
            'content' => $newContent,


        ] )->assertJsonFragment( [
            'message' =>
                'Answer updated successfully.'

        ] );
        $this->assertDatabaseHas( 'answers', [
            'content' => $newContent,
        ] );

    }

    /**
     * Delete test
     *
     * @return void
     */
    public function testDelete()
    {

        Passport::actingAs(
            $user = factory( User::class )->create()
        );
        $answer = factory( Answer::class )->create();


        $this->delete( '/api/languages/' . $answer->story->language->slug . '/stories/' . $answer->story->id.'/answers/'.$answer->id )->assertJsonFragment( [
            'message' =>
                'Answer deleted successfully.'

        ] );
        $this->assertDatabaseMissing( 'answers', [
            'id' => $answer->id,
        ] );
    }
}
