/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './views/App'
import Register from './views/Register'
import Login from './views/Login'
import Home from './views/Home'
import Language from './views/Language'
import Stories from './views/Stories'
import Story from './views/Story'
import Dashboard from './views/Dashboard'
import AddStory from "./views/AddStory";
import axios from 'axios'
import Vuelidate from 'vuelidate'

var base = axios.create({
    baseURL: 'http://writer-recommender.test/api',
    headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token')
    }
});
Vue.use(Vuelidate);
Vue.prototype.$http = base;

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/dashboard',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
        },
        {
            path: '/languages/:slug',
            name: 'languageView',
            component: Language,
            props: true
        },
        {
            path: '/languages/:slug/stories/',
            name: 'stories',
            component: Stories,
            props: true
        },
        {
            path: 'users/:user_id/languages/:slug/stories/',
            name: 'user-stories',
            component: Stories,
            props: true
        },
        {
            path: '/languages/:slug/stories/:id/',
            name: 'storyView',
            component: Story,
            props: true
        },
        {
            path: '/languages/:slug/add-story/:id?',
            name: 'storyNew',
            component: AddStory,
            props: true,
            meta: {
                requiresAuth: true
            }
        },

    ],
});
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('token') === null || localStorage.getItem('token') === 'undefined') {
            next({
                path: '/login',
                params: {nextUrl: to.fullPath}
            })
        } else {
            next()
        }
    } else {
        next()
    }
});
base.interceptors.request.use(
    (config) => {
        let token = localStorage.getItem('token');
        if (token) {
            config.headers['Authorization'] = `Bearer ${ token }`;
        }

        return config;
    },

    (error) => {
        return Promise.reject(error);
    }
);

const app = new Vue({
    el: '#app',
    components: {App},
    router,
    base
});
