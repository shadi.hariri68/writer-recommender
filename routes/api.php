<?php

use Illuminate\Http\Request;

Route::post( 'register', 'Api\AuthController@register' )->name( 'api.register' );
Route::post( 'login', 'Api\AuthController@login' )->name( 'api.login' );
Route::post( 'logout', 'Api\AuthController@logout' )->name( 'api.logout' )->middleware( 'auth:api' );;


Route::group( [ 'namespace' => 'Api' ], function () {
    Route::apiResource( 'languages', 'LanguageController' );
    Route::group( [ 'prefix' => 'languages/{language}' ], function () {
        Route::apiResource( 'stories', 'StoryController' );
        Route::group( [ 'prefix' => 'stories/{story}' ], function () {
            Route::apiResource( 'answers', 'AnswerController' );

        } );

    } );
    Route::group( [ 'middleware' => [ 'auth:api' ] ], function () {

    } );
} );

Route::middleware( 'auth:api' )->get( '/user', function ( Request $request ) {
    return $request->user();
} );
